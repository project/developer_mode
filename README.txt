                                                                                
General Information
-------------------
Limit access to the Drupal admin panel for your clients. Block functionality like updating content and viewing menu items for administrators.

If you develop Drupal websites for your clients, this is the plugin for you! Developer Mode makes it possible to easily disable certain parts of the admin panel for your clients, while keeping full control over all admin panel functionality for yourself and other developers. For example, you can hide certain menu items for non-developer users and even stop them from creating content by showing Content Freeze message.

Main options
Hide admin menu items for non-developer users, but not for yourself.
Avoid user to create/edit any content.
Disable core, module or theme updates for non-developer users.